# Makefile
#
# Licensed under GNU General Public License v3.0.

FILES=src/main.vala
ARGS=--pkg posix --pkg gio-2.0
VALAC=valac
OUTFILE=unityx-session

out/$(OUTFILE): clean
	@echo 'Compiling...'
	mkdir out
	valac $(ARGS) $(FILES) --output='out/$(OUTFILE)'

.PHONY: test
test: out/$(OUTFILE)
	@echo ''
	@echo 'Running tests...'
	@out/unityx-session &>/dev/null && { echo "Failed test: No arguments" ;} || echo "Successful test: No arguments."

.PHONY: clean
clean:
	@rm -rf out
